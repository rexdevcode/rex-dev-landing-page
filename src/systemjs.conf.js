'use strict';

(function(global) {
    // ENV
    global.ENV = global.ENV || 'development';

    // map tells the System loader where to look for things
    var map = {
        'app': 'src/tmp/app',
        'test': 'src/tmp/test'
    };

    // packages tells the System loader how to load when no filename and/or no extension
    var packages = {
         'app': { defaultExtension: 'js' },
        'test': { defaultExtension: 'js' },
        'rxjs': { defaultExtension: 'js' },
        'angular2-google-maps/core': { defaultExtension: 'js' }
    };

    // List bower packages here
    var bowerPackages = [
        'foundation-sites',
        'motion-ui',
        'jquery',
        'what-input',
        'isotope',
        'owl.carousel'
    ];

    // List npm packages here
    var npmPackages = [
        '@angular',
        'angular2-in-memory-web-api',
        'rxjs',
        'angular2-google-maps'
    ];

    // Add package entries for packages that expose barrels using index.js
    var n_packageNames = [
        // 3rd party barrels
        'angular2-in-memory-web-api',
        'angular2-google-maps/core'
    ];


    // Add package entries for angular packages
    var ngPackageNames = [
        'common',
        'compiler',
        'core',
        'http',
        'platform-browser',
        'platform-browser-dynamic',
        'router',
        "router-deprecated"
    ];

    bowerPackages.forEach(function(pkgName) {
      map[pkgName] = `bower_modules/${pkgName}`;
    });

    npmPackages.forEach(function (pkgName) {
        map[pkgName] = `node_modules/${pkgName}`;
    });

    n_packageNames.forEach(function(pkgName) {
      packages[pkgName] = { main: `index.js`, defaultExtension: 'js' };
    });

    bowerPackages.forEach(function(pkgName) {
      packages[pkgName] = { main: `/dist/${pkgName}.min.js`, defaultExtension: 'js' };
    });

    ngPackageNames.forEach(function(pkgName) {
        var main = global.ENV === 'testing' ? 'index.js' :
            'bundles/' + pkgName + '.umd.js';

        packages['@angular/'+pkgName] = { main: main, defaultExtension: 'js' };
    });

    var config = {
        map: map,
        packages: packages
    };

    // filterSystemConfig - index.html's chance to modify config before we register it.
    if (global.filterSystemConfig) { global.filterSystemConfig(config); }

    System.config(config);

})(this);
