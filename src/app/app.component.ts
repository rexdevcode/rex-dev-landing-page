// Se eliminana las config de el router
import {Component} from '@angular/core';
import {HomeComponent} from './home/home.component';
import {EmailService}         from './services/email.service';

@Component({
    selector: 'as-main-app',
    directives: [HomeComponent],
    templateUrl: 'app/app.html',
    providers: [EmailService]
})
export class AppComponent {

}
