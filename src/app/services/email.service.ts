import {Injectable}  from '@angular/core';
import {Http}              from '@angular/http';
import {Observable}        from 'rxjs/Observable';
import 'rxjs/Rx';

declare var ENV: any;

@Injectable()
export class EmailService {

  private baseAPI: string;

  constructor(private http: Http) {
    if(ENV === 'production') {
      this.baseAPI = 'localhost:3000';
    } else {
      this.baseAPI = 'http://rexdeveloper.com';
    }
  }

  send(mail):Observable<any> {
    return this.http.post(`${this.baseAPI}/php/mail.php`, JSON.stringify(mail))
                    .map(res => res.json())
                    .share();
  }

}
