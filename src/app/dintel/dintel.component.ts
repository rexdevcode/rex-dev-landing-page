import {Component, ElementRef, AfterViewInit, Input} from '@angular/core';
import {APP_ROUTES} from '../app.routes';
import {NavbarComponent} from '../navbar/navbar.component';
import {Observable} from 'rxjs/Rx';

declare const foundation: any;
declare const jQuery: any;

@Component({
  selector: 'dintel',
  templateUrl: 'app/dintel/dintel.html',
  directives: [NavbarComponent]
})

export class DintelComponent implements AfterViewInit {
  @Input() fondo:any;
  @Input() arrow:any;
  public appRoutes: any[];
  public appBrand: string;
  clickButtons: any;
  clickButtons$: Observable<any>;

  constructor(private elementRef: ElementRef) {
      this.appRoutes = APP_ROUTES;
      this.appBrand = 'RexDev';
  }

  getWidth(): number {
    return window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
  }

  getHeigth(): number {
    return window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
  }

  ngAfterViewInit() {
    document.getElementById('arrow').style.backgroundImage = `url(${this.arrow})`;
    document.getElementById('dintel-background').style.backgroundImage = `url(${this.fondo})`;

    const resizeDintel = () => {
      document.getElementById('dintel-background').style.height = (this.getHeigth())+'px';
      document.getElementById('callout').style.padding = (((this.getHeigth() / 2 ) - 220 ) +'px 0px');
    };
    let $window = jQuery(window);
    $window.on('scroll resize', resizeDintel);

    this.clickButtons = document.getElementsByTagName('a');
    this.clickButtons$ = Observable.fromEvent(this.clickButtons,'click');

    this.clickButtons$.subscribe((subject)=>{
      if(subject.srcElement.className === 'menu-tag')
        document.getElementById('example-menu').style.display = 'none';
    });
  }

}
