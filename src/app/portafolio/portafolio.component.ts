import {Component} from '@angular/core';

@Component({
  selector: 'portafolio',
  templateUrl: 'app/portafolio/portafolio.html'
})

export class PortafolioComponent {
  public lastWork: any[] = [
    {
      'nombre': 'Nupp',
      'classes': {'element-item': true, 'grafic-design':true, 'design-web':true, 'app-movil':true},
      'tipo': 'Diseño web + Imagen corporativo',
      'img': 'assets/images/portafolio/portafolio3.png',
      'desc': 'Empresa Tecnologica'
    },
    {
      'nombre': 'Skönhet',
      'classes': {'element-item': true, 'grafic-design':true, 'design-web':false,  'app-movil':false},
      'tipo': 'Imagen corporativo',
      'img': 'assets/images/portafolio/portafolio2.png',
      'desc': 'Salon de belleza'
    },
    {
      'nombre': 'Mi Pueblito',
      'classes': {'element-item': true, 'grafic-design':true, 'design-web':false, 'app-movil':false},
      'tipo': 'Imagen corporativo',
      'img': 'assets/images/portafolio/portafolio4.png',
      'desc': 'Restaurante Buffete'
    },
    {
      'nombre': 'H20 FC',
      'classes': {'element-item': true, 'grafic-design':false, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + backend',
      'img': 'assets/images/portafolio/portafolio6.png',
      'desc': 'Club de futbol'
    },
    {
      'nombre': 'Iris',
      'classes': {'element-item': true, 'grafic-design':true, 'design-web':false, 'app-movil':false},
      'tipo': 'Imagen corporativo',
      'img': 'assets/images/portafolio/portafolio1.png',
      'desc': 'Salon de belleza'
    },
    {
      'nombre': 'LEC',
      'classes': {'element-item': true, 'grafic-design':false, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + backend',
      'img': 'assets/images/portafolio/portafolio7.png',
      'desc': 'Empresa internacional de impuestos'
    },
    {
      'nombre': 'ITM',
      'classes': {'element-item': true, 'grafic-design':false, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + backend',
      'img': 'assets/images/portafolio/portafolio5.png',
      'desc': 'Universidad de estudios superiores'
    },
    {
      'nombre': 'New Port',
      'classes': {'element-item': true, 'grafic-design':false, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + backend',
      'img': 'assets/images/portafolio/portafolio8.png',
      'desc': 'Instituto de lenguas'
    },
    {
      'nombre': 'La Valiente',
      'classes': {'element-item': true, 'grafic-design':true, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + Imagen corporativo',
      'img': 'assets/images/portafolio/portafolio9.png',
      'desc': 'Pyme de alimentos'
    },
    {
      'nombre': 'Red Mexlter',
      'classes': {'element-item': true, 'grafic-design':false, 'design-web':true, 'app-movil':false},
      'tipo': 'Diseño web + backend',
      'img': 'assets/images/portafolio/portafolio10.png',
      'desc': 'Plataforma de investigación del ecosistema atmosferica'
    }
  ];
}
