import {Component, Input, AfterViewInit} from '@angular/core';
import {FormBuilder,FORM_DIRECTIVES,
        ControlGroup,
        Validators}           from '@angular/common';
import {EmailService}         from '../services/email.service';
import {CustomValidators}     from '../validators/custom-validators';
import { GOOGLE_MAPS_DIRECTIVES } from 'angular2-google-maps/core';

@Component({
    selector: 'contacto-form',
    templateUrl: 'app/contacto/contacto-form.html',
    directives: [FORM_DIRECTIVES
      ,GOOGLE_MAPS_DIRECTIVES
    ]
})

export class ContactoComponent implements AfterViewInit {

  @Input() fondo: string;
  public group: ControlGroup;
  public formulario:any = {};
  public mensaje: string;

  lat: number = 19.688727;
  lng: number = -101.146679;
  zoom: number = 15;

  constructor(private _builder: FormBuilder, private _emailService: EmailService) {

    this.formulario = {
      nombre: '',
      email: '',
      asunto: '',
      telefono: '',
      mensaje: ''
    };

    this.group = this._builder.group({
      nombre: ['',
        Validators.compose([Validators.required, CustomValidators.textFormat])
      ],
      email: ['',
        Validators.compose([Validators.required, CustomValidators.emailFormat])
      ],
      asunto: ['',
        Validators.compose([Validators.required, CustomValidators.textFormat])
      ],
      telefono: ['',
        Validators.compose([Validators.required, CustomValidators.telefonoFormat])
      ],
      mensaje: ['',
        Validators.compose([Validators.required, CustomValidators.textAreaFormat])
      ]
    });
  }

  ngAfterViewInit() {
    document.getElementById('contacto-background').style.backgroundImage = `url(${this.fondo})`;
  }

  submitForm() {
    this._emailService.send(this.formulario).subscribe(
      (data) => this.mensaje = data.mensaje,
      (err)  => console.log(err),
      ()     => {
        this.formulario = {
          nombre: '',
          email: '',
          asunto: '',
          telefono: '',
          mensaje: ''
        };
      }
    );
  }


}
