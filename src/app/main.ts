import {bootstrap} from '@angular/platform-browser-dynamic';
import {enableProdMode}   from '@angular/core';
import {HTTP_PROVIDERS}   from '@angular/http';
import {AppComponent} from './app.component';
import {GOOGLE_MAPS_PROVIDERS} from 'angular2-google-maps/core';

declare var ENV: string;

if (ENV === 'production') {
    enableProdMode();
}

bootstrap(AppComponent,[
  HTTP_PROVIDERS,
  GOOGLE_MAPS_PROVIDERS
]
).catch(err => console.error(err));
