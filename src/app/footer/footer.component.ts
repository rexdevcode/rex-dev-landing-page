import {Component} from '@angular/core';

@Component({
    selector: 'footer',
    templateUrl: 'app/footer/footer.html'
})

export class FooterComponent {
  public fecha: any;

  constructor() {
    this.fecha = new Date();
    this.fecha = this.fecha.getFullYear();
  }

}
