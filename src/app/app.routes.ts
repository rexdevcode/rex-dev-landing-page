import {HomeComponent}     from './home/home.component';
import {ContactoComponent} from './contacto/contacto.component';
import {NosotrosComponent} from './nosotros/nosotros.component';
import {ServiciosComponent} from './servicios/servicios.component';

export var APP_ROUTES:any[] = [
    { path: '/', name: '#Inicio', component: HomeComponent, useAsDefault: true},
    { path: '/nosotros', name: '#Nosotros', component: NosotrosComponent },
    { path: '/servicios', name: '#Servicios', component: ServiciosComponent },
    { path: '/portafolio', name: '#Portafolio', component: NosotrosComponent },
      { path: '/contacto', name: '#Contacto', component: ContactoComponent }
];
