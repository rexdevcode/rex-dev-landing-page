import {Component,ElementRef, OnInit, AfterViewInit } from '@angular/core';
import {DintelComponent}     from '../dintel/dintel.component';
import {NosotrosComponent}   from '../nosotros/nosotros.component';
import {ServiciosComponent}  from'../servicios/servicios.component';
import {PortafolioComponent} from '../portafolio/portafolio.component';
import {FooterComponent}     from '../footer/footer.component';
import {ContactoComponent}   from '../contacto/contacto.component';
import {APP_ROUTES} from '../app.routes';

declare var jQuery:any;
declare var foundation:any;
declare var owlCarousel:any;

@Component({
    selector: 'home',
    templateUrl: 'app/home/home.html',
    directives: [
                  DintelComponent,
                  NosotrosComponent,
                  PortafolioComponent,
                  ServiciosComponent,
                  FooterComponent,
                  ContactoComponent
              ]
})
export class HomeComponent implements OnInit, AfterViewInit {

  public fondoHome: string = 'assets/images/covers/home.jpg';
  public fondoContacto: string = 'assets/images/covers/home2.jpg';
  public arrow: string = 'assets/images/iconos/arrow-down.png';
  public appR: any[];

  constructor(private elementRef: ElementRef) {
    this.appR = APP_ROUTES;
  }

  ngOnInit() {
    jQuery(this.elementRef.nativeElement).foundation();
  }

  ngAfterViewInit() {

    jQuery('#owl-demo-home').owlCarousel({
      autoPlay: 3000, // Set AutoPlay to 3 seconds
      stopOnHover : true,
      navigation:true,
      items : 1,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3]
    });

    // Efecto
    let $animation_elements = jQuery('.animation-element');
    let $window = jQuery(window);

    function check_if_in_view() {
      let window_height = $window.height();
      let window_top_position = $window.scrollTop();
      let window_bottom_position = (window_top_position + window_height);

      jQuery.each($animation_elements, function() {
        let $element = jQuery(this);
        let element_height = $element.outerHeight();
        let element_top_position = $element.offset().top;
        let element_bottom_position = (element_top_position + element_height);

        // check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
            $element.addClass('in-view');
        } else {
          $element.removeClass('in-view');
        }
      });
    }
    $window.on('scroll resize', check_if_in_view);
    $window.trigger('scroll');

    // init Isotope
    let $grid = jQuery('.grid').isotope({
      itemSelector: '.element-item',
      layoutMode: 'fitRows',
    });

    // filter functions
    let filterFns = {
      // show if number is greater than 50
      numberGreaterThan50: function() {
        let number = jQuery(this).find('.number').text();
        return parseInt( number, 10 ) > 50;
      },
      // show if name ends with -ium
      ium: function() {
        let name = jQuery(this).find('.name').text();
        return name.match( /ium$/ );
      }
    };
    // bind filter button click
    jQuery('.filters-button-group').on( 'click', 'button', function() {
      let filterValue = jQuery( this ).attr('data-filter');
      // use filterFn if matches value
      filterValue = filterFns[ filterValue ] || filterValue;
      $grid.isotope({ filter: filterValue });
    });
    // change is-checked class on buttons
    jQuery('.button-group').each( function( i, buttonGroup ) {
      let $buttonGroup = jQuery( buttonGroup );
      $buttonGroup.on( 'click', 'button', function() {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        jQuery( this ).addClass('is-checked');
      });
    });
  }

}
