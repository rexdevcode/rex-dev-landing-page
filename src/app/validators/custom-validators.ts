import {Control} from '@angular/common';

export class CustomValidators {

  static textFormat(control: Control) {
    let pattern:RegExp = /^[a-zA-Z0-9?\s]{3,}$/;
    return pattern.test(control.value) ? null : {'textFormat': true};
  }
  static textAreaFormat(control: Control) {
    let pattern:RegExp = /^[a-zA-Z0-9?$@#()'!,+\-=_:.&€£*%\s]{15,}$/;
    return pattern.test(control.value) ? null : {'textFormat': true};
  }
  static emailFormat(control: Control) {
    let pattern:RegExp = /\S+@\S+\.\S+/;
    return pattern.test(control.value) ? null : {'emailFormat': true};
  }
  static telefonoFormat(control: Control) {
  let pattern:RegExp = /^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$/;
  return pattern.test(control.value) ? null : {'telefonoFormat': true};
  }
}
