'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const $     = require('gulp-load-plugins')();
const config = require('../gulp.config')();

gulp.task('sass',  () => {
    return gulp.src(config.assetsPath.styles + 'main.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe($.autoprefixer({
          browsers: ['last 2 versions']
        }))
        .pipe(gulp.dest(config.assetsPath.styles));
});

gulp.task('watch-sass', done => {
    gulp.watch([config.sassFiles.app,config.sassFiles.static],
               ['sass','serve-reload']);
    done();
});
