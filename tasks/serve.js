'use strict';

const gulp             = require('gulp');
const notify           = require('gulp-notify');
const config           = require('../gulp.config')();
const bs               = require("browser-sync").create();
const startBrowsersync = (config,done) => {
    bs.init(config);
    done();
}

/* Start live server dev mode */
gulp.task('serve-dev', ['sass', 'tsc-app', 'watch-ts', 'watch-sass'], (done) => {
    startBrowsersync(config.browserSync.dev,done);
});

/* Start live server production mode */
gulp.task('serve-build', ['build'], (done) => {
    startBrowsersync(config.browserSync.prod,done);
});

// Tarea para reiniciar servidor y observar los cambios
gulp.task('serve-reload', (done) => {
  bs.reload();
  done();
});
