'use strict';

const gulp = require('gulp');
const config = require('../gulp.config')();
const del = require('del');

/* Run all clean tasks */
gulp.task('clean', ['clean-build', 'clean-report', 'clean-ts', 'clean-sass']);

/* Clean build folder */
gulp.task('clean-build', () => {
    return del([config.build.path]);
});

/* Clean report folder */
gulp.task('clean-report', () => {
    return del([config.report.path]);
});

/* Clean sass compile */
gulp.task('clean-sass', () => {
    return del([config.assetsPath.styles + '**/*.css',config.app+'**/*.css']);
});

/* Clean js and map */
gulp.task('clean-ts', () => {
    return del([config.tmp, config.app+'**/*.component.js', config.app+'**/*.component.js.map']);
});

gulp.task('clean-ts-app', () => {
    return del([config.tmpApp, config.app+'**/*.component.js', config.app+'**/*.component.js.map']);
});

gulp.task('clean-ts-test', () => {
    return del([config.tmpTest]);
});
