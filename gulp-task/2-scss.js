'use strict';
const gulp   = require('gulp');
const $      = require('gulp-load-plugins')();
const config = require('../gulp.config')();
const isProduction = false;

// Compiles Sass
gulp.task('sass', () => {
  const minifyCss = $.if(isProduction, $.minifyCss());

  return gulp.src(config.assetsPath.styles + 'main.scss')
    .pipe($.sass({
      outputStyle: (isProduction ? 'compressed' : 'nested'),
      errLogToConsole: true
    }))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie 10']
    }))
    .pipe(minifyCss)
    .pipe(gulp.dest(config.assetsPath.styles));
});
