'use strict';
const gulp     = require('gulp');
const config   = require('../gulp.config')();

gulp.task('watch', (done) => {

// Observando cambios en los archivos TS
  gulp.watch(
    [config.src+'**/*.ts', `!${config.src}**/*.js`, `!${config.src}**/*.js.map`],
    gulp.series('tsc-app', 'clean-map', 'serve-reload')
  );

// Observando cambios en los archivos SCSS
  gulp.watch(
    [config.sassFiles.app,config.sassFiles.static],
    gulp.series(
      'sass','serve-reload'
    ));

  done();

});
