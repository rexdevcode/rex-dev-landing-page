'use strict';
const gulp       = require('gulp');
const ts         = require('gulp-typescript');
const tslint     = require('gulp-tslint');
const sourcemaps = require('gulp-sourcemaps');
const config     = require('../gulp.config')();

const tsConfig    = ts.createProject('tsconfig.json');
const tsUnitFiles = [].concat(config.tsTestFiles.unit, config.tsTestFiles.helper);
const tsE2EFiles  = [].concat(config.tsTestFiles.e2e, config.tsTestFiles.helper);
const tsFiles     = [].concat(config.tsFiles, tsUnitFiles, tsE2EFiles);

 function compileTs(files, watchMode) {

   watchMode = watchMode || false;
   const allFiles = [].concat(files, config.typingFiles);

   return gulp.src(allFiles)
           .pipe(tslint())
           .pipe(tslint.report('prose', {
             summarizeFailureOutput: true,
             emitError: !watchMode
            }))
           .pipe(sourcemaps.init())
           .pipe(ts(tsConfig))
           .pipe(sourcemaps.write(config.tmp))
           .pipe(gulp.dest(config.tmp));
 }


function lintTS(files) {
  return gulp.src(files)
    .pipe(tslint())
    .pipe(tslint.report('prose', {
      summarizeFailureOutput: true
    }));
}

/* Lint typescripts */
gulp.task('tslint', () => {
    return lintTs(tsFiles);
});

/* Compile typescripts */
gulp.task('tsc-app', () => {
  return compileTs(config.tsFiles);
});
