'use strict';
const gulp   = require('gulp');
const notify = require('gulp-notify');
const bs     = require('browser-sync').create('server');
const config = require('../gulp.config')();

gulp.task('serve-reload', (done) => {
  bs.reload();
  done();
});

gulp.task('serve-dev',
  gulp.series('clean', 'sass', 'tsc-app', (done) => {
  bs.init(config.browserSync.dev);
  done();
}));

gulp.task('serve-build',
  gulp.series('clean','build', (done) => {
  bs.init(config.browserSync.prod);
  done();
}));

bs.emitter.on('init', () => gulp.src('.').pipe(notify('Server is running!')));
