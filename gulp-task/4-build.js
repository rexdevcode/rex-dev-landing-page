'use strict';
const gulp = require('gulp');
const runSequence = require('run-sequence');
const config = require('../gulp.config')();
const useref = require('gulp-useref');
const gulpif = require('gulp-if');
const rev = require('gulp-rev');
const revReplace = require('gulp-rev-replace');
const uglify = require('gulp-uglify');
const cssnano = require('gulp-cssnano');
const Builder = require('systemjs-builder');

const cssStyles = [
  config.app + '**/*.css',
  'bower_components/components-font-awesome/css/font-awesome.min.css',
  'bower_components/mdi/css/materialdesignicons.min.css',
  'bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
  'bower_components/owl.carousel/dist/assets/owl.theme.default.min.css'
];

/* Copy fonts in packages */
gulp.task('fonts', () => {
    gulp.src(config.assetsPath.fonts + '**/*.*', {
        base: config.assetsPath.fonts
    })
    .pipe(gulp.dest(config.build.fonts));

    gulp.src([
        'bower_components/components-font-awesome/fonts/*.*',
        'bower_components/mdi/fonts/*.*'
    ])
    .pipe(gulp.dest(config.build.fonts));
});


gulp.task('build-systemjs',
  gulp.series('tsc-app', (done) => {
    const builder = new Builder();
    builder.loadConfig(config.src + 'systemjs.conf.js')
    .then(function() {
        const path = config.tmpApp;
        const b = builder
            .buildStatic(
                path + 'main.js',
                path + 'bundle.js',
                config.systemJs.builder);
        console.log(b);
        return b;
    })
    .then(function() {
        console.log('Build complete');
        done();
    })
    .catch(function (ex) {
        console.log('error', ex);
        done('Build failed.');
    });
}));

/* Concat and minify/uglify all css, js, and copy fonts */
gulp.task('build-assets',
  gulp.series('clean-build', gulp.parallel('sass','fonts'), (done) => {

        gulp.src(config.app + '**/*.html', {
            base: config.app
        })
        .pipe(gulp.dest(config.build.app));

        gulp.src(cssStyles, {
            base: config.root
        })
        .pipe(cssnano())
        .pipe(gulp.dest(config.build.app));

        gulp.src(config.src + 'favicon.ico')
        .pipe(gulp.dest(config.build.path));

        gulp.src(config.assetsPath.images + '**/*.*', {
            base: config.assetsPath.images
        })
        .pipe(gulp.dest(config.build.assetPath + 'images'));

        gulp.src(config.index)
            .pipe(useref())
            // .pipe(gulpif('assets/lib.js', uglify()))
            .pipe(gulpif('*.css', cssnano()))
            .pipe(gulpif('!*.html', rev()))
            .pipe(revReplace())
            .pipe(gulp.dest(config.build.path))
            .on('finish', done);
    }));


gulp.task('build',
  gulp.series('build-systemjs', 'build-assets'), (done)  => {
    done();
  });
