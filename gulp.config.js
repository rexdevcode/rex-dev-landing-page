'use strict';
const historyApiFallback = require('connect-history-api-fallback')

module.exports = function () {
  // rutas de directorios del proyecto
    const root       = '';
    const src        = root + 'src/';
    const app        = src + 'app/';
    const test       = src + 'test/';
    const tmp        = src + 'tmp/';
    const tmpApp     = tmp + 'app/';
    const tmpTest    = tmp + 'test/';
    const testHelper = test + 'test-helpers/';
    const e2e        = test + 'e2e/';
    const tmpE2E     = tmpTest + 'e2e/';
    const phpFiles   = src + 'php/';
    const assets     = src + 'assets/';
    const assetsPath = {
        styles: assets + 'styles/',
        images: assets + 'images/',
        fonts:  assets + 'fonts/'
    };

    // rutas de archivos principales
    const index      = src + 'index.html';
    const tsFiles    = [
        app + '**/!(*.spec)+(.ts)'
    ];
    const tsTestFiles = {
        unit: [app + '**/*.spec.ts'],
        e2e:  [e2e + '**/*.ts'],
        helper: [testHelper + '**/*.ts']
    };
    const typingFiles = [
        'typings/index.d.ts',
        'manual_typings/**/*.d.ts'
    ];
    const sassFiles = {
            app: `${app}**/*.scss`,
         static: `${assetsPath.styles}**/*.scss`
    };

    // rutas del proyecto puesto a produccion
    const build = {
        path:      'build/',
        app:       'build/app/',
        fonts:     'build/fonts',
        phpFiles:  'build/php/',
        assetPath: 'build/assets/',
        assets: {
            lib: {
                js:  'lib.js',
                css: 'lib.css'
            }
        }
    };
    // ruta de reporte de produccion
    const report = {
        path: 'report/'
    };

    // configuracion de browserSync
    const browserSync = {
        dev: {
            port: 3000,
            server: {
                baseDir: './src/',
                middleware: [historyApiFallback()],
                routes: {
                    "/node_modules":     "node_modules",
                    "/bower_components": "bower_components",
                    "/src":              "src"
                }
            },
            files: [
                src    + "index.html",
                src    + "systemjs.conf.js",
                src    + "assets/styles/main.css",
                tmpApp + "**/*.js",
                app    + "**/*.css",
                app    + "**/*.html",
                phpFiles + '**/*.php'
            ]
        },
        prod: {
            port: 3001,
            server: {
                baseDir: './' + build.path,
                middleware: [historyApiFallback()]
            }
        }
    };

    // configuracion de selenium
    const e2eConfig = {
        seleniumTarget: 'http://127.0.0.1:3000'
    };

    // configuracionde de minificacion de systemJs
    const systemJs = {
        builder: {
            normalize: true,
               minify: true,
               mangle: true,
              runtime: false,
           globalDefs: { DEBUG: false, ENV: 'production' }
        }
    };

    // Json de configuracion del proyecto
    const config = {
              root: root,
               src: src,
               app: app,
              test: test,
               tmp: tmp,
            tmpApp: tmpApp,
           tmpTest: tmpTest,
            tmpE2E: tmpE2E,
        testHelper: testHelper,
               e2e: e2e,
         e2eConfig: e2eConfig,
          phpFiles: phpFiles,
            assets: assets,
             index: index,
             build: build,
            report: report,
        assetsPath: assetsPath,
           tsFiles: tsFiles,
       tsTestFiles: tsTestFiles,
       typingFiles: typingFiles,
         sassFiles: sassFiles,
       browserSync: browserSync,
          systemJs: systemJs
    };

    return config;
};
